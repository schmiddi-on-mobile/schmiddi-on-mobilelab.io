---
layout: flare
title: Flare
description: Chat with your friends on Signal
---

Flare is an unofficial app that lets you chat with your friends on Signal from Linux. It is still in development and doesn't include as many features as the official Signal applications.

![](https://gitlab.com/schmiddi-on-mobile/flare/-/raw/master/data/screenshots/screenshot.png)

It is available for install on [Flathub](https://flathub.org/apps/details/de.schmidhuberj.Flare). You can find installation instructions for other platforms and methods [here](https://gitlab.com/schmiddi-on-mobile/flare#installation).

If you [have an issue with Flare](https://gitlab.com/schmiddi-on-mobile/flare/-/issues/new), would like to [suggest an improvement](https://gitlab.com/schmiddi-on-mobile/flare/-/issues/new?templete=feature_request), or would like to [contribute one yourself](https://gitlab.com/schmiddi-on-mobile/flare#contributing), the source code is available [here](https://gitlab.com/schmiddi-on-mobile/flare).

We also have a [Matrix room](https://matrix.to/#/#flare-signal:matrix.org) where you can discuss Flare with us!
