---
layout: default
title: Home
---

We're a group dedicated to creating beautiful apps for your mobile Linux devices. All our apps are built with GTK4 and Libadwaita, and are adaptive for both desktop and mobile Linux devices!

We are the developers of [Flare](/flare), [Pipeline](/pipeline), and [Railway](/railway).

You can find our apps over on [our GitLab group](https://gitlab.com/schmiddi-on-mobile).

If you'd like to get in touch with us, we have a [Matrix space](https://matrix.to/#/#schmiddionmobile:matrix.org).

We're also on [Mastodon](https://fosstodon.org/@schmiddionmobile).
