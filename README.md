# Our Website

## Licensing information
The [Anchor Headings](https://github.com/allejo/jekyll-anchor-headings) library (MIT) and [FreeSans](https://www.gnu.org/software/freefont/) font (GPL3) are under the licenses of their developers. This repository's license only applies to the code for the repository itself.

If you believe your code has been unjustly used or has been used without proper credit in this repository, please contact us.

## Self-hosting information
If you are going to host a site like this yourself, it is requested that you remove our content from it and replace it with your own. This includes references to our name, projects, or website.

You must remove all folders except the `_layouts`, `_includes`, and `assets` folders and all files except the `_config.yml`, `LICENSE.txt`, and `README.md` files.

You must also modify the `_layouts/default.html`, `_config.yml`, and `README.md` files to remove our name.

You will need to add your own files for your website to have any content. Creating an `index.md` or `index.html` file will create a home page.

You will also need to modify the `_layouts/default.html` file to include your pages in the navigation section.

Under the terms of the [GNU General Public License v3.0](http://choosealicense.com/licenses/gpl-3.0/) license used by this project, you are required to use this same license in any and all redistributions of this project's code.

If you wish to host a site like this on GitHub Pages instead of GitLab Pages, you can do so without issue, as no changes made impact this compatibility.
