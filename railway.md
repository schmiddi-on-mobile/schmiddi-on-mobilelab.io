---
layout: railway
title: Railway
description: Travel with all your train information in one place
---

Railway lets you look up travel information for many different railways, all without needing to navigate through different websites.

![](https://gitlab.com/schmiddi-on-mobile/railway/-/raw/master/data/screenshots/overview.png)

It is available for install on [Flathub](https://flathub.org/apps/details/de.schmidhuberj.DieBahn). You can find installation instructions for other platforms and methods [here](https://gitlab.com/schmiddi-on-mobile/railway#installation).

If you [have an issue with Railway](https://gitlab.com/schmiddi-on-mobile/railway/-/issues/new), would like to suggest an improvement, or would like to contribute one yourself, the source code is available [here](https://gitlab.com/schmiddi-on-mobile/railway).

We also have a [Matrix room](https://matrix.to/#/#railwayapp:matrix.org) where you can discuss Railway with us!
