---
layout: pipeline
title: Pipeline
description: Watch YouTube and PeerTube videos in one place
---

Pipeline lets you watch and download videos from YouTube and PeerTube, all without needing to navigate through different websites.

![](https://gitlab.com/schmiddi-on-mobile/pipeline/-/raw/master/data/screenshots/feed.png)

It is available for install on [Flathub](https://flathub.org/apps/details/de.schmidhuberj.tubefeeder). You can find installation instructions for other platforms and methods [here](https://gitlab.com/schmiddi-on-mobile/pipeline#installation).

If you [have an issue with Pipeline](https://gitlab.com/schmiddi-on-mobile/pipeline/-/issues/new), would like to suggest an improvement, or would like to [contribute one yourself](https://gitlab.com/schmiddi-on-mobile/pipeline/-/blob/master/CONTRIBUTING.md), the source code is available [here](https://gitlab.com/schmiddi-on-mobile/pipeline).

We also have a [Matrix room](https://matrix.to/#/#pipelineapp:matrix.org) where you can discuss Pipeline with us!
